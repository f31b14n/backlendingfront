from enum import Enum, unique


@unique
class Status(Enum):

    DECLINED = 'Declined'
    UNDECIDED = 'Undecided'
    APPROVED = 'Approved'

    @staticmethod
    def values():
        return list(map(lambda e: e.value, Status))
