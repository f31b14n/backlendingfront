from flask import Blueprint, request
from flask_restplus import Api, Resource, fields
from app.constants import Status
from app.use_cases import ApplicationApproval


module_controller = Blueprint('module_controller', __name__)
api = Api(module_controller, doc='/docs')


@api.route('/health')
class HealthCheck(Resource):

    def get(self):
        return {}


model_request = api.model('RequestApplication', {
    'identifier': fields.Integer(
        description='Tax Id',
        min=100,
        required=True
    ),
    'name': fields.String(
        description='Business Name',
        min_length=3,
        required=True
    ),
    'request_amount': fields.Integer(
        description='Requested amount',
        required=True,
        min=1000
    ),
})
model_result = api.model('ResultApplication', {
    'uuid': fields.String(
        description='Application Id'
    ),
    'status': fields.String(
        attribute='status.value',
        description='Status of the Application',
        enum=Status.values(),
    )
})


@api.route(
    '/v1/application',
    doc={'description': 'Approve or rejected the application'}
)
class Application(Resource):

    @api.expect(model_request, validate=True)
    @api.marshal_with(model_result, code=201)
    @api.response(400, 'Validation Error')
    def post(self):
        use_case = ApplicationApproval()
        return use_case.handle(request.get_json()), 201
