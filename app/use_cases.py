import uuid
from app.constants import Status


class ResultApplication:

    def __init__(self, app_uuid, status):
        self.uuid: str = app_uuid
        self.status: str = status


class ApplicationApproval:

    _amount = 50000

    def handle(self, request: dict) -> ResultApplication:
        amount = request['request_amount']
        app_uuid = str(uuid.uuid1())

        if amount > self._amount:
            return ResultApplication(app_uuid, Status.DECLINED)
        if amount == self._amount:
            return ResultApplication(app_uuid, Status.UNDECIDED)
        return ResultApplication(app_uuid, Status.APPROVED)
