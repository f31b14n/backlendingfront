import pytest
from functools import wraps
from app import create_app


@pytest.fixture
def app():
    app = create_app()
    return app


@pytest.fixture(scope="class")
def client(request):
    app = create_app()
    test_client = app.test_client()

    def teardown():
        # databases and resourses have to be freed at the end.
        # But so far we don't have anything
        pass

    request.addfinalizer(teardown)

    request.cls.client = test_client
