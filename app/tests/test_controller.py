import pytest
import json
from unittest import TestCase
from unittest.mock import patch, Mock
from app.constants import Status


@pytest.mark.usefixtures('client')
class TestHealth(TestCase):

    def test_ok(self):
        response = self.client.get('/health')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.data), {})


@pytest.mark.usefixtures('client')
class TestApplication(TestCase):

    @patch('app.controller.ApplicationApproval')
    def test_ok(self, mock_use_case):
        mock_handle = Mock()
        mock_handle.handle.return_value = Mock(
            status=Status.APPROVED,
            uuid='abc-123'
        )
        mock_use_case.return_value = mock_handle

        response = self.client.post('/v1/application', json={
            "identifier": 123,
            "name": "a name",
            "request_amount": 30000
        })

        self.assertEqual(response.status_code, 201)
        self.assertEqual(json.loads(response.data), {
            'status': Status.APPROVED.value,
            'uuid': 'abc-123'
        })
        mock_handle.handle.assert_called_with({
            "identifier": 123,
            "name": "a name",
            "request_amount": 30000
        })
