from flask.app import Flask
from unittest import TestCase
from app import create_app


class TestApp(TestCase):

    def test_create_app(self):
        app = create_app()

        self.assertIsInstance(app, Flask)
