from unittest import TestCase
from unittest.mock import patch
from app.constants import Status
from app.use_cases import ResultApplication, ApplicationApproval


class TestCreateAffiliate(TestCase):

    _amount = 50000

    @patch('app.use_cases.uuid.uuid1', return_value='abc-123')
    def test_declined(self, mock_uuid1):
        use_case = ApplicationApproval()
        result = use_case.handle({
            'request_amount': self._amount + 1
        })

        self.assertIsInstance(result, ResultApplication)
        self.assertEqual(result.status, Status.DECLINED)
        self.assertEqual(result.uuid, 'abc-123')
        mock_uuid1.assert_called_with()

    @patch('app.use_cases.uuid.uuid1', return_value='abc-567')
    def test_approved(self, mock_uuid1):
        use_case = ApplicationApproval()
        result = use_case.handle({
            'request_amount': self._amount - 1
        })

        self.assertIsInstance(result, ResultApplication)
        self.assertEqual(result.status, Status.APPROVED)
        self.assertEqual(result.uuid, 'abc-567')
        mock_uuid1.assert_called_with()

    @patch('app.use_cases.uuid.uuid1', return_value='abc-0098')
    def test_undecided(self, mock_uuid1):
        use_case = ApplicationApproval()
        result = use_case.handle({
            'request_amount': self._amount
        })

        self.assertIsInstance(result, ResultApplication)
        self.assertEqual(result.status, Status.UNDECIDED)
        self.assertEqual(result.uuid, 'abc-0098')
        mock_uuid1.assert_called_with()
