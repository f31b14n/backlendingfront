from flask import Flask
from flask_cors import CORS
from flask_restplus import Api


# extensions
api = Api()
cors = CORS(resources={r"/*": {"origins": "*"}})


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile('config.py', silent=True)

    # register modules Blueprint
    from app.controller import module_controller
    app.register_blueprint(module_controller)

    # extensions init_app
    api.init_app(app)
    cors.init_app(app)

    return app
