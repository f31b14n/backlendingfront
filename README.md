# Test Backend by Fabián Salinas #

Backend a project Flask


## Virtual Enviroment
- Create
    ```shell
    $ virtualenv --python=python3.7 venv
    // or
    $ python3 -m venv venv
    ```
- Activate
    ```shell
    $ source venv/bin/activate
    ```
- Deactivate
    ```shell
    $ deactivate
    ```


## Initialize project
- Install dependences
    ```shell
    $ pip3 install -r requirements.txt
    ```
- Prepare config
    ```shell
    $ source prepare.sh
    ```


## Run App
```shell
$ flask run
```


## Documentations
```
http://127.0.0.1:5000/docs
```

## UnitTest
Written tests with `pytest`. [See more](https://docs.pytest.org/en/latest)

- Run test
    ```shell
    $ pytest
    ```
- Show coverage of test
    ```shell
    $ pytest --cov=.
    ```
